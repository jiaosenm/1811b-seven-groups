
```
  bw-project  
   ├─ .env.development                开发环境  
   ├─ .env.production                 生产环境
   ├─ .eslintrc.js                    ESList 配置
   ├─ .huskyrc                        上传git commit 规范
   ├─ .lintstagedrc                   代码规范命令
   ├─ babel.config.js                  
   ├─ commitlint.config.js            commit 规范
   ├─ package-lock.json               
   ├─ package.json  
   ├─ postcss.config.js         
   ├─ prettierrc.js                   prettierrc 代码风格配置
   ├─ public    
   │  ├─ favicon.ico
   │  └─ index.html
   ├─ README.md
   ├─ src
   │  ├─ api                          接口配置
   │  │  ├─ config
   │  │  │  ├─ gangwei.js
   │  │  │  ├─ interview.js
   │  │  │  ├─ login.js
   │  │  │  ├─ questionDetail.js
   │  │  │  └─ xiangmu.js
   │  │  └─ index.js
   │  ├─ App.vue            
   │  ├─ assets                       静态资源
   │  │  ├─ img
   │  │  │  ├─ footer.png
   │  │  │  ├─ loading.gif
   │  │  │  ├─ login-background.55b26da3.jpg
   │  │  │  └─ logo.png
   │  │  ├─ questionDetail.css
   │  │  └─ tailwindcss.css
   │  ├─ components                   组件
   │  │  ├─ ElBreadcrumb.vue          
   │  │  ├─ Professional.vue
   │  │  ├─ SuperTables.vue
   │  │  └─ TableRender.vue         
   │  ├─ main.js                      
   │  ├─ plugins                      插件
   │  │  └─ index.js
   │  ├─ router                       路由
   │  │  ├─ index.js                  
   │  │  ├─ routerConfig.js           路由表
   │  │  ├─ routesConfig.js           
   │  │  └─ whiteList.js              
   │  ├─ store                        
   │  │  └─ index.js
   │  ├─ utils                        公共工具        
   │  │  ├─ elementUi.js              element-plus
   │  │  ├─ httpCode.js
   │  │  └─ httptool.js               axios二次封装
   │  └─ views                        视图
   │     ├─ Login.vue                 登录页
   │     ├─ Teachers  
   │     │  ├─ Defence.vue
   │     │  ├─ InterviewList.vue
   │     │  ├─ InterviewManage.vue
   │     │  ├─ PlanList.vue
   │     │  ├─ PlanListManage.vue
   │     │  ├─ Postm.vue
   │     │  ├─ PostmDetail.vue
   │     │  ├─ Projectm.vue
   │     │  ├─ QuestionDetail.vue
   │     │  ├─ QuestionHandle.vue
   │     │  ├─ RankList.vue
   │     │  └─ ViewPlan.vue
   │     └─ Teachers.vue              
   ├─ tailwind.config.js              tailwindCss 配置表
   └─ vue.config.js                   vue-cli 配置项

```