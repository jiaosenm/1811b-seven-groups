import api from "../../api/index";
const { interview } = api;
export default {
  state: () => ({
    navData: [],
  }),
  actions: {
    getNavData: async ({ commit }) => {
      await interview.interview().then(({ data }) => {
        commit("setNavData", data);
      });
    },
  },
  mutations: {
    setNavData(state, payload) {
      state.navData = payload;
    },
  },
};
