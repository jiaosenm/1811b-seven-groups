import { createStore } from "vuex";

import classList from "./modules/classList";
import navTitleData from "./modules/NavTitleData";

const store = createStore({
  state: {
    majorName: "", //专业
    nickName: "",
    interviewRankList: [],
  },
  mutations: {
    GET_USER_MAJORNAME(state, payload) {
      state.majorName = payload;
    },
    GET_USER_NICKNAME(state, payload) {
      state.nickName = payload;
    },
    GET_INTERVIER_RANK(state, payload) {
      state.interviewRankList = payload;
    },
  },
  actions: {},
  modules: {
    classList,
    navTitleData,
  },
});

window.store = store;
export default store;
