//获取问答列表数据
export const reply = {
  url: "/dev-api/sypt/answer/reply",
  method: "get",
};
export const getList = {
  url: "/dev-api/sypt/answer/list",
  method: "get",
};
export const getTypeList = {
  url: "/dev-api/sypt/project/name/list",
  method: "get",
};
