const routes = [
  {
    path: "/",
    redirect: "login",
  },
  // 教师
  {
    path: "/teachers",
    name: "Teachers",
    component: () => import("../views/Teachers.vue"),
    children: [
      {
        path: "/teachers",
        redirect: "/teachers/postm",
      },
      // 岗位
      {
        path: "postm",
        name: "Postm",
        component: () => import("../views/Teachers/Postm.vue"),
      },
      // 添加岗位
      {
        path: "postmadd",
        name: "PostmAdd",
        component: () => import("../views/Teachers/PostmAdd.vue"),
      },
      {
        path: "project",
        name: "project",
        component: () => import("../views/Teachers/Project/Project.vue"),
        meta: {
          title: "项目 / 项目管理",
        },
      },
      {
        path: "addProjects",
        name: "addProjects",
        component: () => import("../views/Teachers/Project/addprojects.vue"),
        meta: {
          title: "添加项目",
        },
      },
      {
        path: "sProject",
        name: "sproject",
        component: () =>
          import("../views/Teachers/Project/student/project.vue"),
        meta: {
          title: "项目",
        },
      },
      {
        path: "projectDetail",
        name: "dproject",
        component: () =>
          import("../views/Teachers/Project/student/projectDetail.vue"),
        meta: {
          title: "项目",
        },
      },
      //添加项目
      {
        path: "addProject",
        name: "addProject",
        component: () => import("../views/Teachers/Project/addProject.vue"),
        meta: {
          title: "添加项目",
        },
      },
      // 岗位详情
      {
        path: "postmDetail/:id",
        name: "PostmDetail",
        component: () => import("../views/Teachers/PostmDetail.vue"),
      },
      // 实训
      {
        path: "training",
        name: "Training",
        component: () => import("../views/Teachers/Training.vue"),
      },
      // 实训详情
      {
        path: "trainingdetail/:id",
        name: "TrainingDetail",
        component: () => import("../views/Teachers/TrainingDetail.vue"),
      },
      // 添加计划
      {
        path: "addPlan",
        name: "AddPlan",
        component: () => import("../views/Teachers/AddPlan.vue"),
      },
      // 实训计划
      {
        path: "planList",
        name: "PlanList",
        component: () => import("../views/Teachers/PlanList.vue"),
      },
      // 实训进度
      {
        path: "viewPlan",
        name: "ViewPlan",
        component: () => import("../views/Teachers/ViewPlan.vue"),
      },
      // 实训答辩
      {
        path: "defence",
        name: "Defence",
        component: () => import("../views/Teachers/Defence.vue"),
      },
      // 实训计划（管理）
      {
        path: "planListManage",
        name: "PlanListManage",
        component: () => import("../views/Teachers/PlanListManage.vue"),
      },
      // 面试记录
      {
        path: "interviewList",
        name: "InterviewList",
        component: () => import("../views/Teachers/InterviewList.vue"),
        meta: { transition: "slide-left" },
      },
      // 面试记录管理
      {
        path: "interviewManage",
        name: "InterviewManage",
        component: () => import("../views/Teachers/InterviewManage.vue"),
        meta: { transition: "slide-right" },
      },
      // 面试排行榜
      {
        path: "rankList",
        name: "RankList",
        component: () => import("../views/Teachers/RankList.vue"),
      },
      // 问答列表
      {
        path: "questionDetail",
        name: "QuestionDetail",
        component: () => import("../views/Teachers/QuestionDetail.vue"),
      },
      // 问答管理
      {
        path: "questionHandle",
        name: "QuestionHandle",
        component: () => import("../views/Teachers/QuestionHandle.vue"),
      },
      //回答详情
      {
        path: "answerDetail",
        name: "AnswerDetail",
        component: () => import("../views/Teachers/AnswerDetail.vue"),
      },
      // 个人中心
      {
        path: "myCenter",
        name: "MyCenter",
        component: () => import("../views/Teachers/MyCenter.vue"),
      },
      // 添加面试
      {
        path: "interviewRecord",
        name: "interviewRecord",
        component: () => import("../views/Teachers/InterviewRecord.vue"),
      },
      {
        path: "interviewDetail/:id",
        name: "InterviewDetail",
        component: () => import("../views/Teachers/InterviewDetail.vue"),
      },
      {
        path: "defenceid/:id",
        name: "Defenceid",
        component: () => import("../views/Teachers/DefenceId.vue"),
      },
      {
        path: "divide/:id",
        name: "Divide",
        component: () => import("../views/Teachers/Divide.vue"),
      },
      {
        path: "terminal",
        name: "Terminal",
        component: () => import("../views/Teachers/Terminal.vue"),
      },
    ],
  },
  // 登录
  {
    path: "/login",
    name: "Login",
    component: () => import("../views/Login.vue"),
  },
];

export default routes;
