import NProgress from "nprogress";
import "nprogress/nprogress.css";
import { createRouter, createWebHistory } from "vue-router";
import routes from "./routerConfig";

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

// 路由守卫;
router.beforeEach((to, from, next) => {
  // 进度条
  NProgress.start();
  let token = localStorage.getItem("token");
  if (!token && to.path !== "/login") {
    next("/login");
  }
  next();
});

// 进度条
router.afterEach(() => {
  NProgress.done();
});

export default router;
