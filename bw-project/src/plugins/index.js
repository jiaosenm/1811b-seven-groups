import router from "@/router";
import store from "@/store";
//插件集
/**
 * @type {import("vue").plugins[]}
 */
const plugins = [router, store];
//批量注册插件
/**
 * @param {import("vue").App} app;
 */
export const usePlugins = (app) =>
  plugins.forEach((plugin) => app.use(plugin), app);
