import axios from "axios";
import code from "./httpCode";
import { ElMessage } from "element-plus";

const Axios = axios.create({
  baseURL: process.env.VUE_APP_BASE_URL,
  // 请求时间，结束请求
  timeout: 10000,
});

// 添加请求拦截器
Axios.interceptors.request.use(
  (config) => {
    // 发送请求前做些什么
    return {
      ...config,
      headers: {
        ...config.headers,
        Authorization: localStorage.getItem("token")
          ? JSON.parse(localStorage.getItem("token"))
          : "",
      },
    };
  },
  (error) => {
    // 对请求错误做些什么
    return Promise.reject(error);
  }
);

// 添加响应拦截器
Axios.interceptors.response.use(
  (response) => {
    // 对响应数据做些什么
    const { data } = response;
    if (data.code === 200) {
      return Promise.resolve(data);
    } else {
      ElMessage.error(data.msg);
      return Promise.reject(data);
    }
  },
  (error) => {
    switch (error.code) {
      case "ECONNABORTED":
        ElMessage.error("您的请求已超时，请重新获取数据");
        break;

      default:
        break;
    }
    code[error.code] && ElMessage.error(code[error.code]);
    return Promise.reject(error);
  }
);

export default Axios;
