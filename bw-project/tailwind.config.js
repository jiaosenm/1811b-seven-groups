module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  purge: [],
  theme: {
    extend: {
      width: {
        600: "25rem",
      },
      height: {
        545: "22.75rem",
        "90s": "90%",
      },
    },
  },
  variants: {},
  plugins: [],
};
